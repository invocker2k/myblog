package Entyti;

public class BaiVIet {
	private int idBaiViet;
	private danhmucbaiviet danhmucs;
	private Image anhbaiViet;
	private String titleBaiViet;
	private String noiDungBaiViet;
	private String ngayThangNam;
	private String noiDungTitle;
	public String getNoiDungTitle() {
		return noiDungTitle;
	}
	public void setNoiDungTitle(String noiDungTitle) {
		this.noiDungTitle = noiDungTitle;
	}
	public int getIdBaiViet() {
		return idBaiViet;
	}
	public void setIdBaiViet(int idBaiViet) {
		this.idBaiViet = idBaiViet;
	}
	
	public danhmucbaiviet getDanhmucs() {
		return danhmucs;
	}
	public void setDanhmucs(danhmucbaiviet danhmucs) {
		this.danhmucs = danhmucs;
	}
	public Image getAnhbaiViet() {
		return anhbaiViet;
	}
	public void setAnhbaiViet(Image anhbaiViet) {
		this.anhbaiViet = anhbaiViet;
	}
	public String getTitleBaiViet() {
		return titleBaiViet;
	}
	public void setTitleBaiViet(String titleBaiViet) {
		this.titleBaiViet = titleBaiViet;
	}
	public String getNoiDungBaiViet() {
		return noiDungBaiViet;
	}
	public void setNoiDungBaiViet(String noiDungBaiViet) {
		this.noiDungBaiViet = noiDungBaiViet;
	}
	public String getNgayThangNam() {
		return ngayThangNam;
	}
	public void setNgayThangNam(String ngayThangNam) {
		this.ngayThangNam = ngayThangNam;
	}
	
}
