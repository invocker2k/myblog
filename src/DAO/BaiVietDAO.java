package DAO;

import java.sql.*;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Entyti.BaiVIet;
import Entyti.Image;
import Entyti.danhmucbaiviet;

public class BaiVietDAO {
		public static List<danhmucbaiviet> GetDanhMuc(Connection conn){
			List<danhmucbaiviet> dMuc=new ArrayList<danhmucbaiviet>();
			String sql="select * from danhmucbaiviet ";
			
			try {
				PreparedStatement ptmt=conn.prepareStatement(sql);
				ResultSet rs=ptmt.executeQuery();
				
				while (rs.next()) {
					danhmucbaiviet dm=new danhmucbaiviet();
					String id=rs.getString("idDanhMuc");
					String ten=rs.getString("tenDanhMuc");
					dm.setIdDanhMuc(Integer.parseInt(id));
					dm.setTenDamhMuc(ten); 
					dMuc.add(dm);
				}
				ptmt.close();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return dMuc;
		}
		public  static List<BaiVIet> TitleBaiViet(Connection conn) {
			String sql="SELECT * from  danhmucbaiviet,baiviet,image where baiviet.idDanhMuc=danhmucbaiviet.idDanhMuc and image.idImage=baiviet.idImage";
			List<BaiVIet> titleBAIVIET=new ArrayList<BaiVIet>();
			try {
				PreparedStatement ptmt=conn.prepareStatement(sql);
				ResultSet rs=ptmt.executeQuery();
				
				while (rs.next()) {
					BaiVIet bvs=new BaiVIet();
					danhmucbaiviet dms=new danhmucbaiviet();
					Image anhs=new Image();
					//
					String tenDanhMuc=rs.getString("tenDanhMuc");
					String id=rs.getString("idBaiViet");
					String title=rs.getString("titleBaiViet");
					String ngay=rs.getString("ngayThangNam");
					String anh=rs.getString("titleImage");
					bvs.setIdBaiViet(Integer.parseInt(id));
					bvs.setTitleBaiViet(title); 
					bvs.setNgayThangNam(ngay);
					dms.setTenDamhMuc(tenDanhMuc);
					anhs.setTitleImage(anh);
					
					bvs.setAnhbaiViet(anhs);
					bvs.setDanhmucs(dms);
					titleBAIVIET.add(bvs);
				}
				ptmt.close();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return titleBAIVIET;
		}
		public static BaiVIet GetSigleBlogById(Connection conn,String id) {
		
			String sql ="select * from baiviet where idBaiViet="+id;
		
			BaiVIet baiViet=new BaiVIet();
			try {
				PreparedStatement ptmt=conn.prepareStatement(sql);
				ResultSet rs=ptmt.executeQuery();
				
				while (rs.next()) {
				
					
					baiViet.setTitleBaiViet( rs.getString("titleBaiViet"));
					baiViet.setNoiDungBaiViet(rs.getString("noiDungBaiViet"));
					baiViet.setNgayThangNam(rs.getString("ngayThangNam"));
				}
			
				ptmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return baiViet;
		}
}
