CREATE DATABASE  IF NOT EXISTS `blog` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `blog`;
-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: blog
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `baiviet`
--

DROP TABLE IF EXISTS `baiviet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `baiviet` (
  `idBaiViet` int(10) NOT NULL AUTO_INCREMENT,
  `idDanhMuc` int(10) DEFAULT NULL,
  `idImage` int(10) DEFAULT NULL,
  `titleBaiViet` varchar(50) DEFAULT NULL,
  `noiDungBaiViet` text,
  `ngayThangNam` varchar(20) DEFAULT NULL,
  `noiDungTitle` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idBaiViet`),
  KEY `baiviet_dm` (`idDanhMuc`),
  KEY `baiviet_image` (`idImage`),
  CONSTRAINT `baiviet_dm` FOREIGN KEY (`idDanhMuc`) REFERENCES `danhmucbaiviet` (`idDanhMuc`),
  CONSTRAINT `baiviet_image` FOREIGN KEY (`idImage`) REFERENCES `image` (`idImage`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `baiviet`
--

LOCK TABLES `baiviet` WRITE;
/*!40000 ALTER TABLE `baiviet` DISABLE KEYS */;
INSERT INTO `baiviet` VALUES (1,1,3,'Đây là 1 dự án Mobile-Android','Pecometer<br>1.Thành viên: 3 người gồm Đặng Thanh Tùng - Hoàng Minh Vũ - Nguyễn văn tân. <br> ','27/2/2020',NULL),(2,1,4,'Đây là 1 dự án web','Page giới thiệu bản thân!<br>Công ngệ sử dung: Ajax, jdbc-Mysql, boostrap 3<br>','28/2/2020',NULL),(3,1,5,'Đây là 1 dự án web bán hàng','Xây dựng trang web bán hàng! <br>Công nghệ xử dụng: Spring mvc, Hibernate, Jquery, boostrap4, Ajax-Ajax lồng<br> Thành viên: Đặng Thanh Tùng.<br>',NULL,NULL);
/*!40000 ALTER TABLE `baiviet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chitiethoadon`
--

DROP TABLE IF EXISTS `chitiethoadon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `chitiethoadon` (
  `mahoadon` int(11) NOT NULL,
  `machitietsanpham` int(11) NOT NULL,
  `soluong` int(11) DEFAULT NULL,
  `giatien` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`mahoadon`,`machitietsanpham`),
  KEY `fk__hd_sp` (`machitietsanpham`),
  CONSTRAINT `fk__hd_sp` FOREIGN KEY (`machitietsanpham`) REFERENCES `chitietsanpham` (`machitietsanpham`),
  CONSTRAINT `fk_chitiet_hd` FOREIGN KEY (`mahoadon`) REFERENCES `hoadon` (`mahoadon`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chitiethoadon`
--

LOCK TABLES `chitiethoadon` WRITE;
/*!40000 ALTER TABLE `chitiethoadon` DISABLE KEYS */;
INSERT INTO `chitiethoadon` VALUES (12,4,4,'1150000 '),(14,1,3,'1150000 '),(14,4,7,'1150000 '),(14,5,4,'1150000 '),(14,6,5,'1150000 '),(16,1,3,'1150000 '),(16,4,7,'1150000 '),(16,5,4,'1150000 '),(16,6,5,'1150000 ');
/*!40000 ALTER TABLE `chitiethoadon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chitietkhuyenmai`
--

DROP TABLE IF EXISTS `chitietkhuyenmai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `chitietkhuyenmai` (
  `makhuyenmai` int(11) NOT NULL,
  `masanpham` int(11) NOT NULL,
  `giamgia` int(11) DEFAULT NULL,
  PRIMARY KEY (`makhuyenmai`,`masanpham`),
  KEY `fk_chitiet_khuyenmai` (`masanpham`),
  CONSTRAINT `fk_chitiet_khuyenmai` FOREIGN KEY (`masanpham`) REFERENCES `sanpham` (`masanpham`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chitietkhuyenmai`
--

LOCK TABLES `chitietkhuyenmai` WRITE;
/*!40000 ALTER TABLE `chitietkhuyenmai` DISABLE KEYS */;
INSERT INTO `chitietkhuyenmai` VALUES (1,1,850000);
/*!40000 ALTER TABLE `chitietkhuyenmai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chitietsanpham`
--

DROP TABLE IF EXISTS `chitietsanpham`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `chitietsanpham` (
  `machitietsanpham` int(11) NOT NULL AUTO_INCREMENT,
  `masanpham` int(11) DEFAULT NULL,
  `masize` int(11) DEFAULT NULL,
  `mamau` int(11) DEFAULT NULL,
  `soluong` int(11) DEFAULT NULL,
  `ngaynhap` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`machitietsanpham`),
  KEY `fk_chitiet_sanpham` (`masanpham`),
  KEY `fk_chitiet_mau` (`mamau`),
  KEY `fk_chitiet_size` (`masize`),
  CONSTRAINT `fk_chitiet_mau` FOREIGN KEY (`mamau`) REFERENCES `mausanpham` (`mamau`),
  CONSTRAINT `fk_chitiet_sanpham` FOREIGN KEY (`masanpham`) REFERENCES `sanpham` (`masanpham`),
  CONSTRAINT `fk_chitiet_size` FOREIGN KEY (`masize`) REFERENCES `sizesanpham` (`masize`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chitietsanpham`
--

LOCK TABLES `chitietsanpham` WRITE;
/*!40000 ALTER TABLE `chitietsanpham` DISABLE KEYS */;
INSERT INTO `chitietsanpham` VALUES (1,1,1,1,100,'18/2/2020'),(2,2,1,1,100,'18/2/2020'),(3,3,1,1,100,'18/2/2020'),(4,1,2,1,100,'22/2/2020'),(5,1,3,1,1,NULL),(6,1,4,1,1,NULL);
/*!40000 ALTER TABLE `chitietsanpham` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chucvu`
--

DROP TABLE IF EXISTS `chucvu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `chucvu` (
  `machucvu` int(11) NOT NULL AUTO_INCREMENT,
  `tenchucvu` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`machucvu`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chucvu`
--

LOCK TABLES `chucvu` WRITE;
/*!40000 ALTER TABLE `chucvu` DISABLE KEYS */;
INSERT INTO `chucvu` VALUES (1,'tung'),(2,'tung'),(3,'tung'),(4,'tung'),(5,'tung'),(6,'tung'),(7,'tung'),(8,'tung'),(9,'tung'),(10,'giamdoc'),(11,'giamdoc'),(12,'giamdoc'),(13,'giamdoc'),(14,'giamdoc'),(15,'giamdoc'),(16,'giamdoc'),(17,'giamdoc'),(18,'giamdoc'),(19,'giamdoc'),(20,'giamdoc'),(21,'giamdoc'),(22,'giamdoc'),(23,'giamdoc');
/*!40000 ALTER TABLE `chucvu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `danhmucbaiviet`
--

DROP TABLE IF EXISTS `danhmucbaiviet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `danhmucbaiviet` (
  `idDanhMuc` int(10) NOT NULL AUTO_INCREMENT,
  `tenDanhMuc` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idDanhMuc`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `danhmucbaiviet`
--

LOCK TABLES `danhmucbaiviet` WRITE;
/*!40000 ALTER TABLE `danhmucbaiviet` DISABLE KEYS */;
INSERT INTO `danhmucbaiviet` VALUES (1,'java'),(2,'Cuộc Sống'),(3,'Android');
/*!40000 ALTER TABLE `danhmucbaiviet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `danhmucsanpham`
--

DROP TABLE IF EXISTS `danhmucsanpham`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `danhmucsanpham` (
  `madanhmuc` int(11) NOT NULL AUTO_INCREMENT,
  `tendanhmuc` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `hinhdanhmuc` text,
  PRIMARY KEY (`madanhmuc`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `danhmucsanpham`
--

LOCK TABLES `danhmucsanpham` WRITE;
/*!40000 ALTER TABLE `danhmucsanpham` DISABLE KEYS */;
INSERT INTO `danhmucsanpham` VALUES (1,'Giày da nam',NULL),(2,'Giày tăng chiều cao ',NULL),(3,'Giày buộc dây nam',NULL),(4,'Giày nam',NULL),(5,'Giầy công sở nam',NULL),(6,'Giầy lười nam',NULL),(7,'Giầy mọi nam',NULL),(8,'Giầy công sở nam',NULL);
/*!40000 ALTER TABLE `danhmucsanpham` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hoadon`
--

DROP TABLE IF EXISTS `hoadon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hoadon` (
  `mahoadon` int(11) NOT NULL AUTO_INCREMENT,
  `tenkhachhang` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `sdt` char(12) DEFAULT NULL,
  `noigiaohang` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `tinhtrang` tinyint(4) DEFAULT NULL,
  `ngaylap` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `hinhthucgiaohang` varchar(45) DEFAULT NULL,
  `ghichu` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`mahoadon`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hoadon`
--

LOCK TABLES `hoadon` WRITE;
/*!40000 ALTER TABLE `hoadon` DISABLE KEYS */;
INSERT INTO `hoadon` VALUES (7,NULL,'03824266660','tai day',0,NULL,'Giao hÃ ng táº­n nÆ¡i','nhanh vao nhe'),(8,NULL,'sedfs','dfs',0,NULL,'Giao hÃ ng táº­n nÆ¡i','dsf'),(9,NULL,'0111100001','Dia chi nay ko co day nhe',0,NULL,'Nháº­n hÃ ng táº¡i cá»­a hÃ ng','ko can giao'),(10,NULL,'0111100001','Dia chi nay ko co day nhe',0,NULL,'Nháº­n hÃ ng táº¡i cá»­a hÃ ng','ko can giao'),(11,'kuhj','uilyoiu','uigoiuy',0,NULL,'Giao hÃ ng táº­n nÆ¡i','uiklhgi'),(12,'kuhj','uilyoiu','uigoiuy',0,NULL,'Giao hÃ ng táº­n nÆ¡i','uiklhgi'),(13,'Äáº·ng Thanh TÃng','','',0,NULL,'Giao hÃ ng táº­n nÆ¡i',''),(14,'Äáº·ng Thanh TÃng','','',0,NULL,'Giao hÃ ng táº­n nÆ¡i',''),(15,'Äáº·ng Thanh TÃng','038242660','báº¯c giang',0,NULL,'Giao hÃ ng táº­n nÆ¡i','Tao Äáº¿n láº¥y'),(16,'Äáº·ng Thanh TÃng','038242660','báº¯c giang',0,NULL,'Giao hÃ ng táº­n nÆ¡i','Tao Äáº¿n láº¥y');
/*!40000 ALTER TABLE `hoadon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image`
--

DROP TABLE IF EXISTS `image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `image` (
  `idImage` int(10) NOT NULL AUTO_INCREMENT,
  `titleImage` varchar(50) DEFAULT NULL,
  `contextImage` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idImage`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image`
--

LOCK TABLES `image` WRITE;
/*!40000 ALTER TABLE `image` DISABLE KEYS */;
INSERT INTO `image` VALUES (1,'javaImg.jpg','1'),(2,'titleProject1.jpg','1'),(3,'project1.png','1'),(4,'project2.png','1'),(5,'project3.png',NULL);
/*!40000 ALTER TABLE `image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `khuyenmai`
--

DROP TABLE IF EXISTS `khuyenmai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `khuyenmai` (
  `makhuyenmai` int(11) NOT NULL AUTO_INCREMENT,
  `tenkhuyenmai` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `thoigianbatdau` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `thoigianketthuc` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `mota` text,
  `hinhkhuyenmai` text,
  PRIMARY KEY (`makhuyenmai`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `khuyenmai`
--

LOCK TABLES `khuyenmai` WRITE;
/*!40000 ALTER TABLE `khuyenmai` DISABLE KEYS */;
INSERT INTO `khuyenmai` VALUES (1,'defau',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `khuyenmai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mausanpham`
--

DROP TABLE IF EXISTS `mausanpham`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mausanpham` (
  `mamau` int(11) NOT NULL AUTO_INCREMENT,
  `tenmau` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`mamau`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mausanpham`
--

LOCK TABLES `mausanpham` WRITE;
/*!40000 ALTER TABLE `mausanpham` DISABLE KEYS */;
INSERT INTO `mausanpham` VALUES (1,'Đen'),(2,'Nâu');
/*!40000 ALTER TABLE `mausanpham` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nhanvien`
--

DROP TABLE IF EXISTS `nhanvien`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `nhanvien` (
  `manhanvien` int(11) NOT NULL AUTO_INCREMENT,
  `hoten` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `diachi` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `gioitinh` bit(1) DEFAULT NULL,
  `cmnd` char(14) DEFAULT NULL,
  `machucvu` int(11) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `tendangnhap` varchar(30) DEFAULT NULL,
  `matkhau` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`manhanvien`),
  KEY `PK_nhanvien_chucvu` (`machucvu`),
  CONSTRAINT `PK_nhanvien_chucvu` FOREIGN KEY (`machucvu`) REFERENCES `chucvu` (`machucvu`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nhanvien`
--

LOCK TABLES `nhanvien` WRITE;
/*!40000 ALTER TABLE `nhanvien` DISABLE KEYS */;
INSERT INTO `nhanvien` VALUES (1,'wretryt','tvbhnkj',_binary '','vbhnj',10,NULL,'invocker','invocker'),(2,'yvbhjnk','bhjnk',_binary '','vbhnjkm',11,NULL,'vbnjm','vgbhnjk'),(3,'dsdgnkmf','nml;,',_binary '','uiim,',12,NULL,'tyuuii','sadssfdg'),(4,'5fghjp','yguhoijoopk',_binary '','iuhiojopk',13,NULL,'r76t78y899i0','67'),(5,'5fghjp','yguhoijoopk',_binary '','iuhiojopk',14,NULL,'r76t78y899i0','67'),(6,'5fghjp','yguhoijoopk',_binary '','iuhiojopk',15,NULL,'r76t78y899i0','tyuytyrewqq'),(7,'xcvbnm','cytvybnkm;l,\';',_binary '','vbnm',16,NULL,'cyrtvuybnkm;l,\';','tytyuybuinm;,\';.'),(8,'xcvbnm','cytvybnkm;l,\';',_binary '','vbnm',17,NULL,'cyrtvuybnkm;l,\';','tytyuybuinm;,\';.'),(9,'xcvbnm','cytvybnkm;l,\';',_binary '','vbnm',18,NULL,'cyrtvuybnkm;l,\';','ewfregtr'),(10,'fcgvbn;m\'','cgvbjnlkm;l,;\'',_binary '','fgvhbkjnklml;l',19,NULL,'gvhbkjnkml','hjkml'),(11,'tfghij','ftyguhijk',_binary '','gyuhijok',20,NULL,'uihij','iuhioj'),(12,'vhbnkm;l','utiui',_binary '','tyuhij',21,NULL,'ftgyuhijl;ok','tfyguhi'),(13,'vygbhnjkml','rftgyhui',_binary '','rftgyhunjim',22,NULL,'rtygbhun','vgbhnjk'),(14,'','',_binary '','',23,'','a','a');
/*!40000 ALTER TABLE `nhanvien` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sanpham`
--

DROP TABLE IF EXISTS `sanpham`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sanpham` (
  `masanpham` int(11) NOT NULL AUTO_INCREMENT,
  `madanhmuc` int(11) DEFAULT NULL,
  `tensanpham` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `giatien` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `mota` text,
  `hinhsanpham` text,
  PRIMARY KEY (`masanpham`),
  KEY `fk_sanpham_danhmucsanpham` (`madanhmuc`),
  CONSTRAINT `fk_sanpham_danhmucsanpham` FOREIGN KEY (`madanhmuc`) REFERENCES `danhmucsanpham` (`madanhmuc`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sanpham`
--

LOCK TABLES `sanpham` WRITE;
/*!40000 ALTER TABLE `sanpham` DISABLE KEYS */;
INSERT INTO `sanpham` VALUES (1,2,'Giày nam tăng chiều cao da bò thật TC02','1150000','Giày được thiết với kiểu dáng thời thượng, phong cách tây. Sự tinh tế của kiếu lười đến từ sự đơn giản về chi tiết trên giày, làm toát lên vẻ đẹp và sự sang trọng của phái mạnh','giay-da-nam-tang-chieu-cao-da-bo-that-tc02.jpg'),(2,2,'Giày tăng chiều cao nam công sở  TC10','1150000','Cao thêm 6cm hoàn toàn bí mật, cho một vóc dáng cao ráo và tự tin trong công việc và giao tiếp, đàm phán hàng ngày.','giay-tang-chieu-cao-nam-cong-so.jpg'),(3,2,'Giày tăng chiều cao nam TC01','850000','Giày tăng chiều cao nam da bò thật TC01 được thiết với kiểu dáng hiện đại. Sự tinh tế của giày đến từ sự đơn giản về chi tiết trên giày, làm toát lên vẻ đẹp và sự sang trọng của phái nam','giay-tang-chieu-cao-nam.jpg'),(4,6,'giày nam da bò GD06','1250000','Giày da bò thật  GD06 mang phong cách doanh nhân thành đạt. Với thiết kế sang trọng đẳng cấp nhưng không kém phần trẻ trung. Được làm hoàn toàn bằng da bò thật mền mịn càng sử dụng càng bền đẹp theo thời gian.','giay-nam-da-bo.jpg'),(5,6,'Giày lười da bò đẹp GD02','599000','Giày nam GD02 mang phong cách doanh nhân thành đạt. Với thiết kế sang trọng đẳng cấp nhưng không kém phần trẻ trung. Được làm hoàn toàn bằng da bò thật mền mịn càng sử dụng càng bền đẹp theo thời gian. ','giay-luoi-nam-da-bo-gd02.jpg'),(7,6,'Giày da bò nam tăng chiều cao TC-09','1150000','Giày tăng chiều cao sẽ giúp bạn cao lên một cách vô hình mà người khác không dễ nhận ra.','giay-da-bo-nam-tang-chieu-cao.jpg'),(8,6,'Giày nam cao cấp GD03','1500000','Giày nam công sở GD03: Mang phong cách cổ điển sang trọng, chất da bò thật 100% mềm mịn đang là sản phẩm rất được ưa chuộng hiện nay của phái mạnh. GD03 sẽ là sự lựa chọn hoàn hảo cho quý ông','giay-da-nam-cong-so-gd03.jpg');
/*!40000 ALTER TABLE `sanpham` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sizesanpham`
--

DROP TABLE IF EXISTS `sizesanpham`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sizesanpham` (
  `masize` int(11) NOT NULL AUTO_INCREMENT,
  `size` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`masize`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sizesanpham`
--

LOCK TABLES `sizesanpham` WRITE;
/*!40000 ALTER TABLE `sizesanpham` DISABLE KEYS */;
INSERT INTO `sizesanpham` VALUES (1,'38'),(2,'39'),(3,'40'),(4,'41'),(5,'42'),(6,'43'),(7,'44');
/*!40000 ALTER TABLE `sizesanpham` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suggestions`
--

DROP TABLE IF EXISTS `suggestions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `suggestions` (
  `idsuggestions` int(11) NOT NULL AUTO_INCREMENT,
  `tenNguoiGopY` varchar(45) DEFAULT NULL,
  `noiDung` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idsuggestions`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suggestions`
--

LOCK TABLES `suggestions` WRITE;
/*!40000 ALTER TABLE `suggestions` DISABLE KEYS */;
INSERT INTO `suggestions` VALUES (1,'gbhjn','bgvhbjjn'),(2,'gbhjn','bgvhbjjn'),(3,'gbhjn','bgvhbjjn'),(4,'1','1'),(5,'1','1'),(6,'1','1'),(7,'1','1'),(8,'1','1'),(9,'1','1'),(10,'1','1'),(11,'1','1'),(12,'1','1'),(13,'1','1'),(14,'tung','áda'),(15,'Đặng Hoàng quốc nhật','Anh Tùng Ơi WEb hỏng rồi'),(16,'Đặng Hoàng quốc nhật','Anh Tùng Ơi WEb hỏng rồi'),(17,'',''),(18,'',''),(19,'',''),(20,'',''),(21,'',''),(22,'',''),(23,'tung @gmail.com',''),(24,'',''),(25,'dang','bjknkkl'),(26,'dang','bjknkkl'),(27,'',''),(28,'a@gmail.com',''),(29,'dsa',''),(30,'q@gmail.com',''),(31,'ákjdkms',''),(32,'a@gmail.com',''),(33,'jadkms',''),(34,'jadkms',''),(35,'a@gmail.com',''),(36,'a@gmail.com',''),(37,'a@gmail.com',''),(38,'invocker@gmail.com','web nhu lol'),(39,'tung@gmail.com','invockerr nha '),(40,'km@fdn.com','ơpik'),(41,'dangtung@gmail.com','cam ơn bạn'),(42,'dangtung@gmail.com','cam ơn bạn');
/*!40000 ALTER TABLE `suggestions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-04  3:44:46
