package Controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.BaiVietDAO;

import java.sql.*;

import DB.DBconnect;
import Entyti.BaiVIet;
import Entyti.danhmucbaiviet;

/**
 * Servlet implementation class MyStoryController
 */
@WebServlet("/MyStoryController")
public class MyStoryController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MyStoryController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Connection conn=DBconnect.CreateConnection();
		//lay danh muc
		List<danhmucbaiviet> dm=BaiVietDAO.GetDanhMuc(conn);
		request.setAttribute("dm", dm);
		// lay title bai viet
		List<BaiVIet> titleBaiviet=BaiVietDAO.TitleBaiViet(conn);
		request.setAttribute("titleBaiViet", titleBaiviet);
		if(conn!=null) {
			
			try {
				conn.close();
				for (danhmucbaiviet danhmucbaiviet : dm) {
				}
				RequestDispatcher requestDispatche=request.getRequestDispatcher("views/menublog.jsp");
				requestDispatche.forward(request, response);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				
				e.printStackTrace();
			}
		}
		else {
			System.out.print("ko thanh cong");

		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
