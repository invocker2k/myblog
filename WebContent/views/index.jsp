<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<c:import url="headerConfig.jsp"></c:import>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
<c:import url="navbarHeader.jsp"></c:import>
<div class="bannerRGB">
        <div class="jumbotron text-center banner">

  			<h1>Đặng Thanh Tùng</h1>
            <p>programmer</p>


          
            <form class="form-inline" action="">
                <input type="text" class="form-control" size="50" placeholder="Nhập Email của bạn">
                <button type="submit" class="btn btn-danger">Đăng Kí Theo Dõi</button>
            </form>
        </div>
    </div>
<!-- noi dung 1-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-8">
                <div class="container-fluid" id="about">
                    <h2>Giới thiệu về Đặng Thanh Tùng</h2>
                    <h4>2021: Tốt nghiệp cao đẳng nghề bách khoa Hà Nội, ngành lập trình máy tính</h4>
                    <p>Yêu java và muốn làm mọi thứ cùng java</p>
                    <br><a href="https://facebook.com/dangtung789"><button class="btn bt-default btn-lg">Liên kết tới facebook của tôi</button></a>
                </div>
            </div>
            <div class="col-sm-4 slideanim">
                <svg class="bi bi-graph-up" width="19em" height="19em" viewBox="0 0 20 20" fill="#1ecbf4" xmlns="http://www.w3.org/2000/svg">
                    <path d="M2 2h1v16H2V2zm1 15h15v1H3v-1z"></path>
                    <path fill-rule="evenodd" d="M16.39 6.312l-4.349 5.437L9 8.707l-3.646 3.647-.708-.708L9 7.293l2.959 2.958 3.65-4.563.781.624z" clip-rule="evenodd"></path>
                    <path fill-rule="evenodd" d="M12 5.5a.5.5 0 01.5-.5h4a.5.5 0 01.5.5v4a.5.5 0 01-1 0V6h-3.5a.5.5 0 01-.5-.5z" clip-rule="evenodd"></path>
                  </svg>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row bg-gray">
            <div class="col-sm-4 slideanim">
                <svg class="bi bi-terminal-fill" width="18em" height="18em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M2 5a2 2 0 012-2h12a2 2 0 012 2v10a2 2 0 01-2 2H4a2 2 0 01-2-2V5zm9.5 5.5h-3a.5.5 0 000 1h3a.5.5 0 000-1zm-6.354-.354L6.793 8.5 5.146 6.854a.5.5 0 11.708-.708l2 2a.5.5 0 010 .708l-2 2a.5.5 0 01-.708-.708z" clip-rule="evenodd"></path>
                  </svg>
            </div>

            <div class="col-sm-8">
                <div class="container-fluid ">
                    <h2>Tôi luôn muốn mang lại giá trị cho người ma tôi gặp</h2>
                    <h4><strong>Tầm nhìn :</strong> Trở thành lập thành viên giỏi trong lĩnh vực developer website</h4>
                    <p><strong>Phong cách :</strong>Là người sống nội tâm, thích làm việc 1 mình. Nhưng đề cao việc nhóm, muốn làm việc tập thể để năng cao kiến thức!</p>

                </div>
            </div>
        </div>
    </div>
    <!-- phan so thich -->
    <div class="container-fluid text-center">
        <h2 id="love">Sở thích của tôi</h2>
        <h4>Sở thích của tôi bao gồm: </h4>
        <br>
        <!--BAT DAU liet ke so thich-->
        <div class="row slideanim">
            <div class="col-sm-4">
                <svg class="bi bi-speaker" width="3em" height="3em" viewBox="0 0 20 20" fill="#1ecbf4" xmlns="http://www.w3.org/2000/svg">
                        <path d="M11 6a1 1 0 11-2 0 1 1 0 012 0zm-2.5 6.5a1.5 1.5 0 113 0 1.5 1.5 0 01-3 0z"></path>
                        <path fill-rule="evenodd" d="M6 2a2 2 0 00-2 2v12a2 2 0 002 2h8a2 2 0 002-2V4a2 2 0 00-2-2H6zm6 4a2 2 0 11-4 0 2 2 0 014 0zm-2 3a3.5 3.5 0 100 7 3.5 3.5 0 000-7z" clip-rule="evenodd"></path>
                      </svg>
                <h4>Lập trình</h4>
                <p>Luôn code khi rảnh</p>
            </div>
            <div class="col-sm-4">
                <svg class="bi bi-lock" width="3em" height="3em" viewBox="0 0 20 20" fill="#1ecbf4" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M13.655 9H6.333c-.264 0-.398.068-.471.121a.73.73 0 00-.224.296 1.626 1.626 0 00-.138.59V15c0 .342.076.531.14.635.064.106.151.18.256.237a1.122 1.122 0 00.436.127l.013.001h7.322c.264 0 .398-.068.471-.121a.73.73 0 00.224-.296 1.627 1.627 0 00.138-.59V10c0-.342-.076-.531-.14-.635a.658.658 0 00-.255-.237 1.123 1.123 0 00-.45-.128zm.012-1H6.333C4.5 8 4.5 10 4.5 10v5c0 2 1.833 2 1.833 2h7.334c1.833 0 1.833-2 1.833-2v-5c0-2-1.833-2-1.833-2zM6.5 5a3.5 3.5 0 117 0v3h-1V5a2.5 2.5 0 00-5 0v3h-1V5z" clip-rule="evenodd"></path>
                      </svg>
                <h4>Công nghệ</h4>
                <p>Yêu công nghệ. Đặc biệt thích nghe những chuyện về bảo mật</p>
            </div>
            <div class="col-sm-4">
                <img src="imgs/iconShoes.png" class="iconLove" alt="icon">
                <h4>Giầy</h4>
                <p>Nhất là giầy da :))</p>
            </div>
        </div>
        <!-- 3 so thich dau-->
        <!--3 so thich sau-->
        <div class="row slideanim">
            <div class="col-sm-4">

                <svg class="bi bi-people" width="3em" height="3em" viewBox="0 0 20 20" fill="pink" xmlns="http://www.w3.org/2000/svg">
  <path fill-rule="evenodd" d="M17 16s1 0 1-1-1-4-5-4-5 3-5 4 1 1 1 1h8zm-7.995-.944v-.002zM9.022 15h7.956a.274.274 0 00.014-.002l.008-.002c-.002-.264-.167-1.03-.76-1.72C15.688 12.629 14.718 12 13 12c-1.717 0-2.687.63-3.24 1.276-.593.69-.759 1.457-.76 1.72a1.05 1.05 0 00.022.004zm7.973.056v-.002zM13 9a2 2 0 100-4 2 2 0 000 4zm3-2a3 3 0 11-6 0 3 3 0 016 0zm-7.064 4.28a5.873 5.873 0 00-1.23-.247A7.334 7.334 0 007 11c-4 0-5 3-5 4 0 .667.333 1 1 1h4.216A2.238 2.238 0 017 15c0-1.01.377-2.042 1.09-2.904.243-.294.526-.569.846-.816zM6.92 12c-1.668.02-2.615.64-3.16 1.276C3.163 13.97 3 14.739 3 15h3c0-1.045.323-2.086.92-3zM3.5 7.5a3 3 0 116 0 3 3 0 01-6 0zm3-2a2 2 0 100 4 2 2 0 000-4z" clip-rule="evenodd"></path>
</svg>
                <h4>Con gái đẹp</h4>
                <p>Mẹ bảo con phải lấy vợ sớm!</p>
            </div>
            <div class="col-sm-4">
                <svg class="bi bi-graph-down" width="3em" height="3em" viewBox="0 0 20 20" fill="#1ecbf4" xmlns="http://www.w3.org/2000/svg">
                        <path d="M2 2h1v16H2V2zm1 15h15v1H3v-1z"></path>
                        <path fill-rule="evenodd" d="M16.39 11.041l-4.349-5.436L9 8.646 5.354 5l-.708.707L9 10.061l2.959-2.959 3.65 4.564.781-.625z" clip-rule="evenodd"></path>
                        <path fill-rule="evenodd" d="M12 11.854a.5.5 0 00.5.5h4a.5.5 0 00.5-.5v-4a.5.5 0 00-1 0v3.5h-3.5a.5.5 0 00-.5.5z" clip-rule="evenodd"></path>
                      </svg>
                <h4>Tài chính</h4>
                <p>Thích đọc sách về marketing</p>
            </div>
            <div class="col-sm-4">
                <svg class="bi bi-reply" width="3em" height="3em" viewBox="0 0 20 20" fill="#1ecbf4" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M11.502 7.013a.144.144 0 00-.202.134V8.3a.5.5 0 01-.5.5c-.667 0-2.013.005-3.3.822-.984.624-1.99 1.76-2.595 3.876 1.02-.983 2.185-1.516 3.205-1.799a8.745 8.745 0 011.921-.306 7.468 7.468 0 01.798.008h.013l.005.001h.001l-.048.498.05-.498a.5.5 0 01.45.498v1.153c0 .108.11.176.202.134l3.984-2.933a.522.522 0 01.042-.028.147.147 0 000-.252.51.51 0 01-.042-.028l-3.984-2.933zM10.3 12.386a7.745 7.745 0 00-1.923.277c-1.326.368-2.896 1.201-3.94 3.08a.5.5 0 01-.933-.305c.464-3.71 1.886-5.662 3.46-6.66 1.245-.79 2.527-.942 3.336-.971v-.66a1.144 1.144 0 011.767-.96l3.994 2.94a1.147 1.147 0 010 1.946l-3.994 2.94a1.144 1.144 0 01-1.767-.96v-.667z" clip-rule="evenodd"></path>
                      </svg>
                <h4>Chia sẻ cảm xúc</h4>
                <p>Thích chia sẻ những gì mình đã đi qua! Những gì đã học.</p>
            </div>
        </div>
        <!-- don3-->
    </div>
    <div id="projects" class="container-fluid text-center  bg-gray">
        <h1 >Dự án tiêu biểu</h1><br>
        <p>Một trong những dự án đã làm</p>
        <!-- projects -->
        <div class="row text-center slideanim">
				 
        <c:forEach var="titlebaiviet" items="${ titleBaiViet}">
          <a href="#hieNoiDungBaiViet">
	          	<div class="col-sm-4 " onclick="HienNoiDungBaiViet(${titlebaiviet.getIdBaiViet() })">
	                <div class="thumbnail">
	                    <img style="height: 230px;" src="imgs/${titlebaiviet.getAnhbaiViet().getTitleImage()}" alt="">
	                    <h4>Dự án ${titlebaiviet.getIdBaiViet() }</h4>
	                    <p>${titlebaiviet.getTitleBaiViet() }</p>
	                </div>
	            </div>
          </a>    
    	</c:forEach>

        </div>
        <div id="hieNoiDungBaiViet" style="text-align: left;display: none;">
        	
        </div>
    </div>
    
    <h1>Mọi người nói gì về tôi?</h1>

    <!--delete-->

    <!---->
    <div id="myCarousel" class="carousel slide text-center" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <h4>"Sao cậu hiền thế"<br><span style="font-style:normal;">1 người bạn cấp 3 từng phát ngôn:))</span></h4>
            </div>
            <div class="item">
                <h4>"anh đen tối "<br><span style="font-style:normal;">Em họ!</span></h4>
            </div>
            <div class="item">
                <h4>" " </span>
                </h4>
            </div>
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!-- Điều Tôi Muốn-->
    <div id="pricing" class="container-fluid  text-center">
        <div class="text-center">
            <h2>Điều tôi mong muốn Trong nghề</h2>
            <h4>Điều mong muốn của tôi tôi bao gồm: </h4>
        </div>

        <br>
        <!--BAT DAU liet ke Điều mong muốn-->
        <div class="row slideanim">
            <div class="col-sm-4 ">
                <div class="panel panel-default text-center">
                    <div class="panel-heading">
                        <h1>Khi nhỏ tôi muốn</h1>
                    </div>
                    <div class="panel-body">
                        <p>bảo mật hệ thống</p>
                        <svg class="bi bi-bookmark-fill" width="2em" height="2em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M5 5a2 2 0 012-2h6a2 2 0 012 2v12l-5-3-5 3V5z" clip-rule="evenodd"></path>
                                  </svg>
                        <h4>Bảo mật hệ thống, quản trị hệ thống</h4>
                      
                    </div>
                    <div class="panel-footer">
                        <p>Chưa được thực hiện</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 ">
                <div class="panel panel-default text-center">
                    <div class="panel-heading">
                        <h1>Lớn dần muốn</h1>
                    </div>
                    <div class="panel-body">
                        <p>Trở thành lập trình viên giỏi</p>
                        <svg class="bi bi-bookmark-fill" width="2em" height="2em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M5 5a2 2 0 012-2h6a2 2 0 012 2v12l-5-3-5 3V5z" clip-rule="evenodd"></path>
                          </svg>
                        <h4>Thích android, Thết kế phần mềm android</h4>
                     
                    </div>
                    <div class="panel-footer">
                        <p>Đang thực hiện</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="panel panel-default text-center">
                    <div class="panel-heading">
                        <h1>Bây giờ muốn</h1>
                    </div>
                    <div class="panel-body">
                        <p>Trở thành lập trình viên giỏi với ngôn ngữ java</p>
                        <svg class="bi bi-bookmark-fill" width="2em" height="2em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M5 5a2 2 0 012-2h6a2 2 0 012 2v12l-5-3-5 3V5z" clip-rule="evenodd"></path>
                          </svg>
                        <h4>Làm mọi thứ với java</h4>
                        <p></p>
                    </div>
                    <div class="panel-footer">
                        <p>Đang được thực hiện</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--xong 3 so thich dau-->
    <div id="googleMap" style="height:400px;width:100%;"></div>
    <!-- Liên hệ-->
    <div id="contact" class="container-fluid">
        <div class="text-center">
            <h2>Liên Hệ với tôi nhé!</h2>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <p>Hãy liên hệ với tôi! Mong rằng tôi có thể giúp được gì cho bạn?</p>
                <div class="diachi">
                    <span class="glyphicon glyphicon-map-marker"></span><span>Hà Nội, VIệt Nam</span>
                </div>
                <div class="sdt">
                   <span class="glyphicon glyphicon-phone"></span><span>0382426669</span>
                </div>
                <div class="emailme">
                   <span class="glyphicon glyphicon-envelope"></span>
                    <span>dangtung789.td@gmail.com</span>
                </div>
            </div>
            <div class="col-sm-7 slideanim">
                <div class="row">
                    <div class="col-sm-6 form-group">
                        <input type="text" class="form-control" placeholder="Tên" name="name">

                    </div>
                    <div class="col-sm-6 form-groupư">
                        <input type="text" class="form-control" placeholder="Email" name="email">

                    </div>
                    <div class="col-sm-12 form-group">
                        <textarea type="text" placeholder="ghi chú" class="form-control"></textarea>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group ">
                            <button class="btn btn-default pull-right form-send" type="submit">Gửi Tung</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <footer class="container-fluid text-center">
        <a href="#myPage" title="Về trang đầu">
            <span class="glyphicon glyphicon-chevron-up"></span>
        </a>
        <p>Cảm ơn tất cả mọi người đã ghé thăm website của mình</p>
    </footer>
</body>

</html>